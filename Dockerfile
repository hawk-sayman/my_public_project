FROM python:3.8-slim-buster

WORKDIR /python-docker

ARG SECRET_NUMBER_ARG
ENV SECRET_NUMBER=$SECRET_NUMBER_ARG

COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt

COPY . .
ENV FLASK_APP=app.py

CMD [ "python3", "-m" , "flask", "run", "--host=0.0.0.0", "--port=8000"]