import os
import datetime
import requests

import pandas as pd
import numpy as np

import sklearn

import mlflow

import mlflow.sklearn
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import mean_squared_error
from sklearn.model_selection import train_test_split
from sklearn import datasets

os.environ["MLFLOW_S3_ENDPOINT_URL"] = "http://65.108.80.242:9000"
os.environ["MLFLOW_TRACKING_URI"] = "http://65.108.80.242:9000"
# os.environ["MLFLOW_ARTIFACT_URI"] = "http://65.108.80.242:9000"

os.environ["AWS_ACCESS_KEY_ID"] = "minio"
os.environ["AWS_SECRET_ACCESS_KEY"] = "minio123"
os.environ["MINIO_ROOT_USER"] = "minio"
os.environ["MINIO_ROOT_PASSWORD"] = "minio123"

mlflow.set_tracking_uri("http://65.108.80.242:5005")
client = mlflow.tracking.MlflowClient()

# print(mlflow.get_artifact_uri()) # This should be '${mlruns_artifacts}/0/${uuid}/artifacts'


experiment = client.get_experiment_by_name("iris_sklearn")
assert experiment is not None, "Не нашелся эксперимент"

iris = datasets.load_iris()
x = iris.data[:, 2:]
y = iris.target

X_train, X_test, y_train, y_test = train_test_split(x, y, test_size=0.2, random_state=7)

with mlflow.start_run(experiment_id=experiment.experiment_id, run_name='iris') as run:
    for num_estimators in [100, 120]:
        with mlflow.start_run(experiment_id=experiment.experiment_id, nested=True) as nested:
            mlflow.log_param("num_estimators", num_estimators)

            rf = RandomForestRegressor(n_estimators=num_estimators)
            rf.fit(X_train, y_train)
            prediction = rf.predict(X_test)

            mlflow.sklearn.log_model(rf, 'random-forest-model', registered_model_name='iris_sklearn')

            mse = mean_squared_error(y_test, prediction)
            mlflow.log_metric('mse', mse)
