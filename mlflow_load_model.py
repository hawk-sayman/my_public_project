import os
import datetime
import requests

import pandas as pd
import numpy as np

import sklearn

import mlflow

import mlflow.sklearn
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import mean_squared_error
from sklearn.model_selection import train_test_split
from sklearn import datasets

os.environ["MLFLOW_S3_ENDPOINT_URL"] = "http://65.108.80.242:9000"
os.environ["MLFLOW_TRACKING_URI"] = "http://65.108.80.242:9000"
# os.environ["MLFLOW_ARTIFACT_URI"] = "http://65.108.80.242:9000"

os.environ["AWS_ACCESS_KEY_ID"] = "minio"
os.environ["AWS_SECRET_ACCESS_KEY"] = "minio123"
os.environ["MINIO_ROOT_USER"] = "minio"
os.environ["MINIO_ROOT_PASSWORD"] = "minio123"

mlflow.set_tracking_uri("http://65.108.80.242:5005")
client = mlflow.tracking.MlflowClient()

MODEL_PATH = 'models:/iris_sklearn/production'
model = mlflow.sklearn.load_model(MODEL_PATH)

x_array = np.array([[1, 1]])
print(model.predict(x_array))
