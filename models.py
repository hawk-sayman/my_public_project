import os
import mlflow

os.environ["MLFLOW_S3_ENDPOINT_URL"] = "http://65.108.80.242:9000"
os.environ["MLFLOW_TRACKING_URI"] = "http://65.108.80.242:9000"

os.environ["AWS_ACCESS_KEY_ID"] = "minio"
os.environ["AWS_SECRET_ACCESS_KEY"] = "minio123"
os.environ["MINIO_ROOT_USER"] = "minio"
os.environ["MINIO_ROOT_PASSWORD"] = "minio123"

MODEL_FLOAT_PATH = "models:/iris_sklearn/production"
MODEL_STRING_PATH = "models:/iris_pyfunc/production"

model_float = mlflow.sklearn.load_model(MODEL_FLOAT_PATH)
model_string = mlflow.pyfunc.load_model(MODEL_STRING_PATH)