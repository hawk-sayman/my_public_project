import os

from minio import Minio
from minio.error import InvalidResponseError

accessID = 'minio'
accessSecret = 'minio123'
minioUrl = "http://65.108.80.242:9001"
bucketName = 'mlflow'

minioUrlHostWithPost = minioUrl.split('//')[1]

s3Client = Minio(
    minioUrlHostWithPost,
    access_key=accessID,
    secret_key=accessSecret,
    secure=False
)

s3Client.make_bucket(bucketName)